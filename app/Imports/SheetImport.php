<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Localidad;
use App\Models\TipoAsentamiento;
use App\Models\Asentamiento;
use App\Models\Zona;

class SheetImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {   
        $estado = Estado::where('clave', $collection[0]['c_estado'])->first();

        if($estado == null):
            $estado = new Estado();
            $estado->clave = $collection[0]['c_estado'];
            $estado->nombre = $collection[0]['d_estado'];
            $estado->save();
        endif;


        foreach($collection as $collec):

            ini_set('max_execution_time', 180);

            $municipio = Municipio::where('clave', $collec['c_mnpio'])
                ->where('estado_id', $estado->id)->first();

            if($municipio == null):
                $municipio = new Municipio();
                $municipio->clave = $collec['c_mnpio'];
                $municipio->nombre = $collec['d_mnpio'];
                $municipio->estado_id = $estado->id;
                $municipio->save();
            endif;

            $localidad = Localidad::where('clave', $collec['c_cve_ciudad'])
                ->where('muncipio_id', $municipio->id)->first();

            if($localidad == null):
                $localidad = new Localidad();
                $localidad->clave = $collec['c_cve_ciudad'];
                $localidad->descripcion = $collec['d_ciudad'];
                $localidad->muncipio_id = $municipio->id;
                $localidad->save();
            endif;
            
            $tipo =  TipoAsentamiento::where('clave', $collec['c_tipo_asenta'])->first();

            if($tipo == null):

                $tipo = new TipoAsentamiento();
                $tipo->clave = $collec['c_tipo_asenta'];
                $tipo->descripcion = $collec['d_tipo_asenta'];
                $tipo->save();

            endif;

            $zona =  Zona::where('descripcion', $collec['d_zona'])->first();

            if($zona == null):
                $zona = new Zona();
                $zona->descripcion = $collec['d_zona'];
                $zona->save();
            endif;

            
            $asent = new Asentamiento();
            $asent->codigo = $collec['d_codigo'];
            $asent->clave = $collec['id_asenta_cpcons'];
            $asent->descripcion = $collec['d_asenta'];
            $asent->tipo_asentamiento_id = $tipo->id;
            $asent->zona_id = $zona->id;
            $asent->localidad_id = $localidad->id;
            $asent->save();
           

        endforeach;
    }
}
