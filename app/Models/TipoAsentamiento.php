<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Asentamiento;

class TipoAsentamiento extends Model
{
    use HasFactory;

    protected $table = 'tipos_asentamientos';


    public function asentamientos()
    {
        return $this->hasMany(Asentamiento::class);
    }
}
