<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Municipio;
use App\Models\Zona;
use App\Models\TipoAsentamiento;
use App\Models\Localidad;

class Asentamiento extends Model
{
    use HasFactory;

    protected $table = 'asentamientos';


    public function localidad()
    {
        return $this->belongsTo(Localidad::class, 'localidad_id');
    }

    public function zona()
    {
        return $this->belongsTo(Zona::class, 'zona_id');
    }

    public function tipoAsentamiento()
    {
        return $this->belongsTo(TipoAsentamiento::class, 'tipo_asentamiento_id');
    }


}
