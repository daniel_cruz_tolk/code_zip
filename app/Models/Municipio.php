<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Estado;
use App\Models\Asentamiento;
use App\Models\Localidad;

class Municipio extends Model
{
    use HasFactory;

    protected $table = "muncipios";

    public function estado()
    {
        return $this->belongsTo(Estado::class, 'estado_id');
    }

    public function localidades()
    {
        return $this->hasMany(Localidad::class);
    }
}
