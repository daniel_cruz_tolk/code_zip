<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Asentamiento;

class Zona extends Model
{
    use HasFactory;

    protected $table = 'zonas';

    public function asentamientos()
    {
        return $this->hasMany(Asentamiento::class);
    }
}
