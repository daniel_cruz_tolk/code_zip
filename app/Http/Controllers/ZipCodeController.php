<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Imports\ZipCodeImport;

use Excel;

class ZipCodeController extends Controller
{
    public function importZipCode()
    {
        #$path = storage_path('CPdescarga.xls');

        $import = new ZipCodeImport();
        $import->sheets();
        $respuesta = Excel::import($import, 'CPdescarga.xls', 'public', \Maatwebsite\Excel\Excel::XLS);
    }
}
