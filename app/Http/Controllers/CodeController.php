<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Asentamiento;

use Str;

class CodeController extends Controller
{
    
    public function getCodeZip($zip_code)
    {
        $quit = ['á','é','í','ó','ú'];
        $replace = ['a','e','i','o','u'];

        $asents = Asentamiento::where('codigo', $zip_code)->get();

        $asenta = $asents->first();

        if($asents->count() > 0):

            $data = [
                "zip_code" => $asenta->codigo,
                "locality" => $asenta->localidad->descripcion == null ? '': Str::upper(str_replace($quit, $replace, $asenta->localidad->descripcion)),
                "federal_entity" => [
                    "key" => $asenta->localidad->municipio->estado->clave,
                    "name" => Str::upper($asenta->localidad->municipio->estado->nombre),
                    "code" => $asenta->localidad->municipio->estado->codigo
                ]
            ];

            $settlements = [];

            foreach($asents as $asent):

                $element = [
                    "key" => intval($asent->clave),
                    "name" => Str::upper($asent->descripcion),
                    "zone_type" => $asent->zona->descripcion,
                    "settlement_type" => [
                        "name" => $asent->tipoAsentamiento->descripcion
                    ]
                ];

                array_push($settlements, $element);

            endforeach;

            $data["settlements"] = $settlements;

            $data["municipality"] = [
                "key" => $asenta->localidad->municipio->clave,
                "name" => Str::upper($asenta->localidad->municipio->nombre)
            ];

            return response()->json($data, 201);

        else:

            return response()->json(["message" => "Codigo no existe!",
                "error" => true,
                "estatus" => 404], 404);

        endif;

    }

}
