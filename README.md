## Solución 

1.- Se analizó y desarrollo una base de datos relacional para guardar la información del catálogo de sepomex, mediante los migrations de Laravel.

    -- ruta de los archivos : database/migrations

2.- Se generaron los modelos y las relaciones a las tablas correspondientes para acceder a la información de manera más rápida mediante el ORM de Laravel.

    -- ruta de los archivos : app/Models

3.- Se desarrolló un método de ETL para guardar la información del archivo .XLSX en la base de datos, mediante Laravel Excel para importar los datos.

    -- ruta de los archivos : app/Imports

4.- Finalmente se desarrolló un método que consultar el código postal y mediante el ORM de Laravel se accedieron a los datos para construir el JSON solicitado.

    -- ruta de los archivos : app/Controllers
